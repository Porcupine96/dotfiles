# shortcut to this dotfiles path is $ZSH
export ZSH=$HOME/.dotfiles

# your project folder that we can `c [tab]` to
export PROJECTS=~/Development

setopt extended_glob

# source every .zsh file in this rep
for config_file ($ZSH/**^external/*.zsh) source $config_file

# use .localrc for SUPER SECRET CRAP that you don't
# want in your public, versioned repo.
if [[ -a ~/.localrc ]]
then
  source ~/.localrc
fi

# initialize autocomplete here, otherwise functions won't be loaded
autoload -U compinit
compinit

# load every completion after autocomplete loads
for config_file ($ZSH/**^external/completion.sh) source $config_file

#if which tmux 2>&1 >/dev/null; then
#  test -z "$TMUX" && tmux new-session && exit
#fi

unsetopt ignoreeof

export VISUAL=vim
autoload edit-command-line; zle -N edit-command-line
bindkey -M vicmd v edit-command-line


# Alias {{{

alias tat='tmux new-session -As $(basename "$PWD" | tr . -)' # will attach if session exists, or create a new session
alias tmuxsrc="tmux source-file ~/.tmux.conf"
alias tmuxkillall="tmux ls | cut -d : -f 1 | xargs -I {} tmux kill-session -t {}" # tmux kill all sessions
alias tmuxkillunnamed="tmux ls | cut -d : -f 1 | grep '\d' | xargs -I {} tmux kill-session -t {}" # tmux kill unnamed sessions

alias notebook="/Users/porcupine/anaconda3/bin/jupyter-notebook --NotebookApp.iopub_data_rate_limit=10000000000"
alias conda="/Users/porcupine/anaconda3/bin/conda"
alias useJava8="export JAVA_HOME=/Library/Java/JavaVirtualMachines/jdk1.8.0_152.jdk/Contents/Home"
alias useJava9="export JAVA_HOME=/Library/Java/JavaVirtualMachines/jdk-9.0.1.jdk/Contents/Home"
alias gpp="git pull && git push"

# }}}

export PATH=$PATH:~/.local/bin

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
alias cat=lolcat
